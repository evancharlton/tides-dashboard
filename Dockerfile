FROM node:12-alpine

WORKDIR /usr/src/app

COPY package.json ./
COPY src/ src
COPY public/ public
COPY yarn.lock yarn.lock
COPY tsconfig.json tsconfig.json

RUN yarn install --frozen-lockfile
RUN yarn build
RUN yarn global add serve

CMD [ "serve", "-n", "-s", "build" ]