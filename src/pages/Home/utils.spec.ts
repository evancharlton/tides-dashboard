import { makePlottable } from './utils';

describe('makePlottable', () => {
  it('handles the empty case', () => {
    expect(makePlottable([])).toEqual([]);
  });

  it('correctly transforms from one to the other', () => {
    expect(
      makePlottable([
        {
          type: 'high',
          time: '30-10-2019 08:31',
          amplitude: 5.96,
          shipments: [
            {
              quantity: 55000,
            },
          ],
        },
        {
          type: 'low',
          time: '30-10-2019 14:40',
          amplitude: 0.42,
          shipments: [],
        },
        {
          type: 'high',
          time: '30-10-2019 20:42',
          amplitude: 6.06,
          shipments: [
            {
              quantity: 43000,
            },
            {
              quantity: 30000,
            },
            {
              quantity: 55000,
            },
          ],
        },
      ])
    ).toEqual([
      {
        amplitude: 5.96,
        datetime: '30-10-2019 08:31',
        numberOfShipments: 1,
      },
      {
        amplitude: 0.42,
        datetime: '30-10-2019 14:40',
        numberOfShipments: 0,
      },
      {
        amplitude: 6.06,
        datetime: '30-10-2019 20:42',
        numberOfShipments: 3,
      },
    ]);
  });
});
