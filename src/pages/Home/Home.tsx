import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import api from 'api';
import { TideRecord, PlottableTideRecord } from 'types';
import styled from 'styled-components';
import { makePlottable } from './utils';
import TideStats from './components/TideStats';
import WeeklySummary from './components/WeeklySummary';
import ShippingTides from './components/ShippingTides';
import TideChart from './components/TideChart';
import { yyyymmdd } from 'utils';
import Tonnage from './components/Tonnage';

const Home = () => {
  const [tideRecords, setTideRecords] = useState<TideRecord[]>([]);
  const [plottableTideRecords, setPlottableTideRecords] = useState<
    PlottableTideRecord[]
  >([]);
  const [title, setTitle] = useState('Tides');

  useEffect(() => {
    api.fetchTideInfo().then(({ items }) => {
      setTideRecords(items);
      setPlottableTideRecords(makePlottable(items));
    });
  }, []);

  const onChangeTimeDomain = (start: Date, end: Date) => {
    setTitle(`Tides | ${yyyymmdd(start)} - ${yyyymmdd(end)}`);
    api.fetchTideInfo({ start, end }).then(({ items }) => {
      setTideRecords(items);
    });
  };

  const onResetTimeDomain = () => {
    setTitle('Tides');
    api.fetchTideInfo().then(({ items }) => {
      setTideRecords(items);
    });
  };

  return (
    <Layout>
      <Helmet>
        <title>{title}</title>
      </Helmet>
      <Contents>
        <TideChart
          records={plottableTideRecords}
          onChangeTimeDomain={onChangeTimeDomain}
          onResetTimeDomain={onResetTimeDomain}
        />
        <WeeklySummary records={tideRecords} />
      </Contents>
      <Sidebar>
        <TideStats records={tideRecords} />
        <ShippingTides records={tideRecords} />
        <Tonnage records={tideRecords} />
      </Sidebar>
    </Layout>
  );
};

const Layout = styled.div`
  display: flex;
  flex-direction: row;
`;

const Contents = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
`;

const Sidebar = styled.div`
  display: flex;
  flex-direction: column;
  min-width: 200px;
  border-left: 2px solid #eee;

  & > div {
    border-bottom: 2px solid #eee;
    padding: 8px 16px;
  }
`;

export default Home;
