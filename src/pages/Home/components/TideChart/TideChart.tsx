import React, { memo, useRef } from 'react';
import { PlottableTideRecord } from 'types';
import Plot from 'react-plotly.js';
import { getTideData, getShipmentData } from './utils';
import { Spin } from 'antd';
import styled from 'styled-components';
import { PlotRelayoutEvent } from 'plotly.js';

type Props = {
  records: PlottableTideRecord[];
  onChangeTimeDomain: (start: Date, end: Date) => void;
  onResetTimeDomain: () => void;
};

const TideChart = ({
  records,
  onResetTimeDomain,
  onChangeTimeDomain,
}: Props) => {
  const data = [getTideData(records), getShipmentData(records)];

  const ref = useRef<HTMLDivElement | null>(null);

  const onRelayout = (event: Readonly<PlotRelayoutEvent>) => {
    const {
      'xaxis.range[0]': start,
      'xaxis.range[1]': end,
      'xaxis.autorange': autorange,
    } = event;

    if (autorange) {
      onResetTimeDomain();
      return;
    }

    if (!!start && !!end) {
      // Changed time domain
      onChangeTimeDomain(new Date(start), new Date(end));
      return;
    }
  };

  let children = null;
  if (records.length === 0) {
    children = <p>No data for this time range</p>;
  } else if (!ref || !ref.current) {
    children = <Spin />;
  } else {
    children = (
      <Plot
        key="stable"
        data={data}
        layout={{
          width: ref.current.clientWidth,
          height: ref.current.clientHeight,
          yaxis: {
            title: 'Tide (m)',
            side: 'right',
          },
          showlegend: false,
        }}
        onRelayout={onRelayout}
      />
    );
  }

  return <Container ref={ref}>{children} </Container>;
};

const Container = styled.div`
  height: 400px;
  text-align: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export default memo(
  TideChart,
  ({ records: oldRecords }, { records: newRecords }) => {
    if (oldRecords.length !== newRecords.length) {
      return false;
    }
    const len = oldRecords.length;
    if (len === 0) {
      return true;
    }
    if (
      oldRecords[0].datetime === newRecords[0].datetime &&
      oldRecords[len - 1].datetime === newRecords[len - 1].datetime
    ) {
      return true;
    }
    return false;
  }
);
