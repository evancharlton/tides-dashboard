import { PlottableTideRecord } from 'types';
import { getMoment } from 'utils';

export const getTideData = (records: PlottableTideRecord[]): Plotly.Data => {
  return records.reduce(
    (acc, record) => {
      return {
        ...acc,
        x: [...acc.x, getMoment(record).toDate()],
        y: [...acc.y, record.amplitude],
      };
    },
    {
      x: [] as Date[],
      y: [] as number[],
      type: 'scatter' as 'scatter',
      mode: 'lines' as 'lines',
      marker: { color: 'blue' },
      line: { shape: 'spline' as 'spline' },
      name: 'Tide (m)',
    }
  );
};

export const getShipmentData = (
  records: PlottableTideRecord[]
): Plotly.Data => {
  return records.reduce(
    (acc, record) => {
      const {
        x,
        y,
        hovertext,
        marker,
        marker: { size },
      } = acc;
      const { numberOfShipments } = record;

      return {
        ...acc,
        x: [...x, getMoment(record).toDate()],
        y: [...y, numberOfShipments ? record.amplitude : null],
        hovertext: [...hovertext, `${numberOfShipments} shipments`],
        marker: {
          ...marker,
          size: [...size, numberOfShipments * 10],
        },
      };
    },
    {
      x: [] as Date[],
      y: [] as (number | null)[],
      hovertext: [] as string[],
      hoverinfo: 'text' as 'text',
      type: 'scatter' as 'scatter',
      mode: 'markers' as 'markers',
      marker: {
        size: [] as number[],
      },
    }
  );
};
