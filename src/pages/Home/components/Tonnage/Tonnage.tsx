import React from 'react';
import { TideRecord } from 'types';
import styled from 'styled-components';
import { Popover } from 'antd';
import EmptyableStatistic from '../EmptyableStatistic';

type Props = {
  records: TideRecord[];
};

const Tonnage = ({ records }: Props) => {
  const { quantity, shipments } = records.reduce(
    ({ quantity, shipments }, record) => {
      return {
        quantity:
          quantity +
          record.shipments.reduce((tot, { quantity }) => tot + quantity, 0),
        shipments: shipments + record.shipments.length,
      };
    },
    { quantity: 0, shipments: 0 }
  );

  let content = <p>There are no shipments during this time</p>;
  if (records.length > 0) {
    content = (
      <p>
        A total of {quantity} tons in {shipments} different shipments
      </p>
    );
  }

  return (
    <Container>
      <Title>Tonnage</Title>
      <Popover content={content} placement="left">
        <StatisticWrapper>
          <EmptyableStatistic
            title="Total"
            value={quantity}
            suffix="tons"
            precision={0}
            emptyText="No shipments"
          />
        </StatisticWrapper>
      </Popover>
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

const Title = styled.h2``;

const StatisticWrapper = styled.div`
  display: inline-block;
`;

export default Tonnage;
