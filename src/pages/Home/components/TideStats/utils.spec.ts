import { computeTideAverages } from './utils';

describe('utils', () => {
  describe('computeTideAverages', () => {
    it('should not crash with no data', () => {
      expect(computeTideAverages([])).toEqual({
        high: undefined,
        low: undefined,
      });
    });

    it('should compute correctly even if there is only one type', () => {
      expect(
        computeTideAverages([
          {
            type: 'high',
            time: '30-10-2019 08:31',
            amplitude: 5.96,
            shipments: [
              {
                quantity: 55000,
              },
            ],
          },
        ])
      ).toEqual({
        high: 5.96,
        low: undefined,
      });
    });

    it('should compute correctly', () => {
      const records = [
        {
          type: 'high',
          time: '30-10-2019 08:31',
          amplitude: 5.96,
          shipments: [
            {
              quantity: 55000,
            },
          ],
        },
        {
          type: 'low',
          time: '30-10-2019 14:40',
          amplitude: 0.42,
          shipments: [],
        },
        {
          type: 'high',
          time: '30-10-2019 20:42',
          amplitude: 6.06,
          shipments: [
            {
              quantity: 43000,
            },
            {
              quantity: 30000,
            },
            {
              quantity: 55000,
            },
          ],
        },
        {
          type: 'low',
          time: '31-10-2019 03:02',
          amplitude: 0.28,
          shipments: [],
        },
        {
          type: 'high',
          time: '31-10-2019 09:08',
          amplitude: 5.71,
          shipments: [],
        },
      ];

      expect(computeTideAverages(records)).toEqual({
        high: 5.91,
        low: 0.35,
      });
    });
  });
});
