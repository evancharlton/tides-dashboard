import { TideRecord } from 'types';

export type TideAverages = {
  high: number | undefined;
  low: number | undefined;
};

export const computeTideAverages = (records: TideRecord[]): TideAverages => {
  const { highCount, highTotal, lowCount, lowTotal } = records.reduce(
    (acc, record) => {
      const isHigh = record.type === 'high';
      const isLow = record.type === 'low';
      return {
        highCount: acc.highCount += isHigh ? 1 : 0,
        highTotal: acc.highTotal += isHigh ? record.amplitude : 0,
        lowCount: acc.lowCount += isLow ? 1 : 0,
        lowTotal: acc.lowTotal += isLow ? record.amplitude : 0,
      };
    },
    { highCount: 0, highTotal: 0, lowCount: 0, lowTotal: 0 }
  );
  return {
    high: highCount ? highTotal / highCount : undefined,
    low: lowCount ? lowTotal / lowCount : undefined,
  };
};
