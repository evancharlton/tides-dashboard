import React, { useMemo } from 'react';
import { TideRecord } from 'types';
import styled from 'styled-components';
import { computeTideAverages } from './utils';
import EmptyableStatistic from '../EmptyableStatistic';

type Props = {
  records: TideRecord[];
};

const TideStats = ({ records }: Props) => {
  const { high, low } = useMemo(() => {
    return computeTideAverages(records);
  }, [records]);

  return (
    <div>
      <Title>Averages</Title>
      <EmptyableStatistic title="High tide" value={high} suffix="m" />
      <EmptyableStatistic title="Low tide" value={low} suffix="m" />
    </div>
  );
};

const Title = styled.h2``;

export default TideStats;
