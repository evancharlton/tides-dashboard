import React from 'react';
import TideStats from './TideStats';
import { render } from 'utils/test';

describe('TideStats', () => {
  it('Renders gracefully with no data', () => {
    const { getAllByText, getByText } = render(<TideStats records={[]} />);
    expect(getAllByText('No data')).toHaveLength(2);
    expect(getByText('Averages')).toBeInTheDocument();
    expect(getByText('High tide')).toBeInTheDocument();
    expect(getByText('Low tide')).toBeInTheDocument();
  });

  it('Renders the right data', () => {
    const records = [
      {
        type: 'high',
        time: '30-10-2019 08:31',
        amplitude: 5.96,
        shipments: [
          {
            quantity: 55000,
          },
        ],
      },
      {
        type: 'low',
        time: '30-10-2019 14:40',
        amplitude: 0.42,
        shipments: [],
      },
      {
        type: 'high',
        time: '30-10-2019 20:42',
        amplitude: 6.06,
        shipments: [
          {
            quantity: 43000,
          },
          {
            quantity: 30000,
          },
          {
            quantity: 55000,
          },
        ],
      },
      {
        type: 'low',
        time: '31-10-2019 03:02',
        amplitude: 0.28,
        shipments: [],
      },
      {
        type: 'high',
        time: '31-10-2019 09:08',
        amplitude: 5.71,
        shipments: [],
      },
    ];

    const { getByText, getByTextWithMarkup } = render(
      <TideStats records={records} />
    );
    expect(getByText('High tide')).toBeInTheDocument();
    expect(getByTextWithMarkup('5.91')).toBeInTheDocument();
  });
});
