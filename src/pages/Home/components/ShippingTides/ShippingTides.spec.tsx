import React from 'react';
import { render } from 'utils/test';
import ShippingTides from './ShippingTides';
import { fireEvent, waitForElement } from '@testing-library/react';
import { act } from 'react-dom/test-utils';

describe('ShippingTides', () => {
  it('handles no data', () => {
    const { getByText } = render(<ShippingTides records={[]} />);
    expect(getByText('No tide information')).toBeInTheDocument();
  });

  it('handles no high tides', () => {
    const records = [
      {
        type: 'low',
        time: '07-11-2019 09:31',
        amplitude: 1.43,
        shipments: [],
      },
    ];

    const { getByText } = render(<ShippingTides records={records} />);
    expect(getByText('No suitable tides')).toBeInTheDocument();
  });

  it('handles no suitable high tides', () => {
    const records = [
      {
        type: 'high',
        time: '06-11-2019 15:02',
        amplitude: 4.83,
        shipments: [],
      },
    ];

    const { getByText } = render(<ShippingTides records={records} />);
    expect(getByText('No suitable tides')).toBeInTheDocument();
    expect(getByText('No assigned shipments')).toBeInTheDocument();
  });

  it('handles no assigned shipments', () => {
    const records = [
      {
        type: 'high',
        time: '08-11-2019 04:16',
        amplitude: 5.25,
        shipments: [],
      },
    ];

    const { getByText, getByTestId } = render(
      <ShippingTides records={records} />
    );
    expect(getByTestId('1 tides')).toBeInTheDocument();
    expect(getByText('No assigned shipments')).toBeInTheDocument();
  });

  it('handles mixed data', async () => {
    const records = [
      {
        type: 'high',
        time: '06-11-2019 15:02',
        amplitude: 4.83,
        shipments: [],
      },
      {
        type: 'low',
        time: '06-11-2019 21:02',
        amplitude: 1.73,
        shipments: [],
      },
      {
        type: 'high',
        time: '07-11-2019 03:21',
        amplitude: 5.02,
        shipments: [
          {
            quantity: 43000,
          },
          {
            quantity: 55000,
          },
        ],
      },
      {
        type: 'low',
        time: '07-11-2019 09:31',
        amplitude: 1.43,
        shipments: [],
      },
    ];

    const { getByText, getByTestId } = render(
      <ShippingTides records={records} />
    );
    expect(getByTestId('1 tides')).toBeInTheDocument();

    const assigned = getByTestId('1 assigned high tides');
    expect(assigned).toBeInTheDocument();

    act(() => {
      fireEvent.mouseOver(assigned);
    });

    await waitForElement(() => getByText('Tides with shipments'));
  });
});
