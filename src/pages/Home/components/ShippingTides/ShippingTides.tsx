import React, { useMemo } from 'react';
import { TideRecord } from 'types';
import styled from 'styled-components';
import { Popover } from 'antd';
import EmptyableStatistic from '../EmptyableStatistic';

type Props = {
  records: TideRecord[];
  highTideLimit?: number;
};

const ShippingTides = ({ records, highTideLimit = 4.9 }: Props) => {
  const { suitableHighTides, highTides, assignedHighTides } = useMemo(() => {
    return records.reduce(
      (acc, record) => {
        if (record.type !== 'high') {
          return acc;
        }
        const { suitableHighTides, highTides, assignedHighTides } = acc;
        const isSuitable = record.amplitude > highTideLimit;
        const hasShipments = record.shipments.length;

        // Question: what to do about tides which are not suitable, yet have
        // shipments assigned anyway? Do they count or not? For the sake of this
        // implementation, I'm assuming they do -- it's better to know that
        // there is a ship coming in than to be surprised.

        return {
          highTides: highTides + 1,
          suitableHighTides: suitableHighTides + (isSuitable ? 1 : 0),
          assignedHighTides: assignedHighTides + (hasShipments > 0 ? 1 : 0),
        };
      },
      { suitableHighTides: 0, highTides: 0, assignedHighTides: 0 }
    );
  }, [records, highTideLimit]);

  const highTidePopover = useMemo(() => {
    if (suitableHighTides === 0) {
      return <p>There are no suitable high tides during this time frame.</p>;
    }

    return (
      <p>
        Out of {highTides} tides in this time frame, {suitableHighTides} are
        high enough to receive shipments.
      </p>
    );
  }, [suitableHighTides, highTides]);

  const assignedHighTidesPopover = useMemo(() => {
    if (suitableHighTides === 0) {
      return <p>There are no suitable high tides during this time frame.</p>;
    }

    if (assignedHighTides === 0) {
      return (
        <p>
          None of the {suitableHighTides} suitable tides have shipments assigned
          to them.
        </p>
      );
    }

    return (
      <p>
        Out of the {suitableHighTides} suitable tides, {assignedHighTides} have
        shipments assigned to them.
      </p>
    );
  }, [suitableHighTides, assignedHighTides]);

  let children = (
    <div>
      <em>No tide information</em>
    </div>
  );
  if (records.length > 0) {
    children = (
      <>
        <Popover placement="left" title="High tides" content={highTidePopover}>
          <StatisticWrapper data-testid={`${suitableHighTides} tides`}>
            <EmptyableStatistic
              title="Suitable tides"
              value={suitableHighTides === 0 ? undefined : suitableHighTides}
              suffix="high tides"
              precision={0}
              emptyText="No suitable tides"
            />
          </StatisticWrapper>
        </Popover>
        <Popover
          placement="left"
          title="Tides with shipments"
          content={assignedHighTidesPopover}
        >
          <StatisticWrapper
            data-testid={`${assignedHighTides} assigned high tides`}
          >
            <EmptyableStatistic
              title="Assigned"
              value={assignedHighTides === 0 ? undefined : assignedHighTides}
              suffix="tides"
              precision={0}
              emptyText="No assigned shipments"
            />
          </StatisticWrapper>
        </Popover>
      </>
    );
  }

  return (
    <Container>
      <Title>Shipping tides</Title>
      {children}
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

const Title = styled.h2``;

const StatisticWrapper = styled.div`
  display: inline-block;
`;

export default ShippingTides;
