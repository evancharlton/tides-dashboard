import React from 'react';
import { Statistic } from 'antd';
import { StatisticProps } from 'antd/lib/statistic/Statistic';

type Props = {
  title: string;
  value: number | undefined;
  emptyText?: string;
} & Omit<StatisticProps, 'value'>;

const EmptyableStatistic = ({
  value,
  emptyText = 'No data',
  title,
  ...rest
}: Props) => {
  if (value === undefined) {
    // Mock out the antd statistic because I'm lazy :)
    return (
      <div className="ant-statistic">
        <div className="ant-statistic-title">{title}</div>
        <div className="ant-statistic-content">
          <span className="ant-statistic-content-value">
            <em className="ant-statistic-content-value-decimal">{emptyText}</em>
          </span>
        </div>
      </div>
    );
  }
  return <Statistic precision={2} {...rest} title={title} value={+value} />;
};

export default EmptyableStatistic;
