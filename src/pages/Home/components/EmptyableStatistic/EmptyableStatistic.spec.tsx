import React from 'react';
import { render } from 'utils/test';
import EmptyableStatistic from './EmptyableStatistic';

describe('EmptyableStatistic', () => {
  it('shows the no-data message correctly', () => {
    const { getByText } = render(
      <EmptyableStatistic title="High tide" value={undefined} />
    );
    expect(getByText('High tide')).toBeInTheDocument();
    expect(getByText('No data')).toBeInTheDocument();
  });

  it('shows numbers correctly', () => {
    const { getByText, getByTextWithMarkup } = render(
      <EmptyableStatistic title="Custom title" value={1.5} />
    );
    expect(getByText('Custom title')).toBeInTheDocument();
    expect(getByTextWithMarkup('1.50')).toBeInTheDocument();
  });
});
