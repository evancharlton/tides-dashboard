import { computeWeeklyBreakdowns } from './utils';

describe('computeWeeklyBreakdowns', () => {
  it('handles no data', () => {
    expect(computeWeeklyBreakdowns([])).toEqual([]);
  });

  it('handles a single report', () => {
    const records = [
      {
        type: 'low',
        time: '31-10-2019 03:02',
        amplitude: 0.28,
        shipments: [],
      },
    ];
    expect(computeWeeklyBreakdowns(records)).toEqual([
      { year: 2019, week: 44, shipments: 0, quantity: 0, key: '2019-44-0' },
    ]);
  });

  it('handles a single week', () => {
    const records = [
      {
        type: 'low',
        time: '31-10-2019 03:02',
        amplitude: 0.28,
        shipments: [],
      },
      {
        type: 'high',
        time: '31-10-2019 09:08',
        amplitude: 5.71,
        shipments: [],
      },
    ];
    expect(computeWeeklyBreakdowns(records)).toEqual([
      { year: 2019, week: 44, shipments: 0, quantity: 0, key: '2019-44-0' },
    ]);
  });

  it('handles splitting weeks', () => {
    const records = [
      {
        type: 'low',
        time: '31-10-2019 03:02',
        amplitude: 0.28,
        shipments: [],
      },
      {
        type: 'high',
        time: '31-10-2019 09:08',
        amplitude: 5.71,
        shipments: [],
      },
      {
        type: 'high',
        time: '06-11-2019 15:02',
        amplitude: 4.83,
        shipments: [],
      },
    ];
    expect(computeWeeklyBreakdowns(records)).toEqual([
      { year: 2019, week: 44, shipments: 0, quantity: 0, key: '2019-44-0' },
      { year: 2019, week: 45, shipments: 0, quantity: 0, key: '2019-45-0' },
    ]);
  });

  it('does the math', () => {
    const records = [
      {
        type: 'low',
        time: '31-10-2019 03:02',
        amplitude: 0.28,
        shipments: [
          {
            quantity: 43000,
          },
          {
            quantity: 55000,
          },
        ],
      },
      {
        type: 'high',
        time: '31-10-2019 09:08',
        amplitude: 5.71,
        shipments: [],
      },
      {
        type: 'high',
        time: '06-11-2019 15:02',
        amplitude: 4.83,
        shipments: [
          {
            quantity: 43000,
          },
          {
            quantity: 30000,
          },
          {
            quantity: 55000,
          },
        ],
      },
    ];
    expect(computeWeeklyBreakdowns(records)).toEqual([
      {
        year: 2019,
        week: 44,
        shipments: 2,
        quantity: 98000,
        key: '2019-44-2',
      },
      {
        year: 2019,
        week: 45,
        shipments: 3,
        quantity: 128000,
        key: '2019-45-3',
      },
    ]);
  });
});
