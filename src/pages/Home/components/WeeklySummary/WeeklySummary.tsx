import React, { useMemo } from 'react';
import { TideRecord } from 'types';
import { computeWeeklyBreakdowns } from './utils';
import { Table } from 'antd';

type Props = {
  records: TideRecord[];
};

const WeeklySummary = ({ records }: Props) => {
  const weeks = useMemo(() => {
    return computeWeeklyBreakdowns(records);
  }, [records]);

  const columns = [
    {
      title: 'Year',
      dataIndex: 'year',
      key: 'year',
    },
    {
      title: 'Week',
      dataIndex: 'week',
      key: 'week',
    },
    {
      title: 'Shipments',
      dataIndex: 'shipments',
      key: 'shipments',
    },
    {
      title: 'Quantity',
      dataIndex: 'quantity',
      key: 'quantity',
    },
  ];

  return <Table dataSource={weeks} columns={columns} pagination={false} />;
};

export default WeeklySummary;
