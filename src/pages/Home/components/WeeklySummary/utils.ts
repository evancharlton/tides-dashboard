import { TideRecord, Shipment } from 'types';
import { getMoment } from 'utils';

export type WeekInfo = {
  year: number;
  week: number;
  quantity: number;
  shipments: number;
};

const totalQuantity = (shipments: Shipment[]): number => {
  return shipments.reduce((acc, shipment) => acc + shipment.quantity, 0);
};

export const computeWeeklyBreakdowns = (records: TideRecord[]): WeekInfo[] => {
  const weekInfos = records.reduce<{
    [yearWeek: string]: WeekInfo;
  }>((acc, record) => {
    const time = getMoment(record);
    const key = `${time.year()}/${time.week()}`;
    const existing = acc[key] || {
      year: time.year(),
      week: time.week(),
      quantity: 0,
      shipments: 0,
    };

    existing.quantity += totalQuantity(record.shipments);
    existing.shipments += record.shipments.length;
    return {
      ...acc,
      [key]: existing,
    };
  }, {});

  return Object.entries(weekInfos)
    .sort(([keyA], [keyB]) => {
      return keyA.localeCompare(keyB);
    })
    .map((entry) => {
      const value = entry[1];
      const { year, week, shipments } = value;
      return {
        ...value,
        key: `${year}-${week}-${shipments}`,
      };
    });
};
