import { TideRecord, PlottableTideRecord } from 'types';

export const makePlottable = (records: TideRecord[]): PlottableTideRecord[] => {
  return records.map((record) => {
    return {
      amplitude: record.amplitude,
      datetime: record.time,
      numberOfShipments: record.shipments.length,
    };
  });
};
