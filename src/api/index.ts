import { TideRecord, ApiResponse } from 'types';
import { getMoment } from 'utils';
import moment from 'moment';
import tides from './tides.json';

type FetchInfo = {
  start?: Date;
  end?: Date;
};

export default {
  fetchTideInfo: async ({ start, end }: FetchInfo = {}): Promise<
    ApiResponse<TideRecord>
  > => {
    if (start && end) {
      const startMoment = moment(start);
      const endMoment = moment(end);
      const { items } = tides;
      const filtered = items.filter((record: TideRecord) => {
        const time = getMoment(record);
        return startMoment.isBefore(time) && endMoment.isAfter(time);
      });
      return {
        ...tides,
        items: filtered,
      };
    }
    return tides;
  },
};
