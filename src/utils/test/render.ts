import React from 'react';
import { render, RenderOptions } from '@testing-library/react';
import withMarkup from './withMarkup';

export default (
  ui: React.ReactElement,
  options?: Omit<RenderOptions, 'queries'>
) => {
  const queries = render(ui, options);
  return {
    ...queries,
    getByTextWithMarkup: withMarkup(queries.getByText),
  };
};
