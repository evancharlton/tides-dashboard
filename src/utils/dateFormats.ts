import moment from 'moment';

export const yyyymmdd = (input: Date | string): string => {
  return moment(input).format('YYYY-MM-DD');
};
