import moment from 'moment';
import { TideRecord, PlottableTideRecord } from 'types';

const isPlottableTideRecord = (
  record: PlottableTideRecord | TideRecord
): record is PlottableTideRecord => {
  return !!(record as PlottableTideRecord).datetime;
};

export default (record: PlottableTideRecord | TideRecord): moment.Moment => {
  let value;
  if (isPlottableTideRecord(record)) {
    value = record.datetime;
  } else {
    value = record.time;
  }
  return moment(value, 'DD-MM-YYYY HH:mm');
};
