export type ApiResponse<T> = {
  items: T[];
};

export type Shipment = {
  /** Tonnage, presumably */
  quantity: number;
};

/** The tide record information as exposed by the API */
export type TideRecord = {
  type: string;
  time: string;
  amplitude: number;
  shipments: Shipment[];
};

/** Tide record information which is suitable for plotting on a chart */
export type PlottableTideRecord = {
  amplitude: number;
  datetime: string;
  numberOfShipments: number;
};
